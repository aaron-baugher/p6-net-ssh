use v6;
use Test;
use lib 'lib';

use-ok('Net::SSH', "can use Net::SSH ok");
use Net::SSH;
my $s;
lives-ok { $s = Net::SSH.new }, "create Net::SSH object";
is($s.ssh-lines('localhost','echo howdy'), 'howdy', 'returns correct string');

done-testing;