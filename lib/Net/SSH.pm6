unit class Net::SSH;

our $VERSION = '0.01';

has $!ssh = 'ssh';
has $.equalspace is rw;
has $!DEBUG = 1;
has @.ssh_options is rw;

method ssh-lines ($host, +@command){
    @.ssh_options = self._ssh_options() unless @.ssh_options;
    my @cmd = flat $!ssh, @.ssh_options, $host, @command;
    warn "[Net::SSH::ssh] executing " ~ join(' ', @cmd) ~ "\n"
        if $!DEBUG;
    my $proc = run(:out, @cmd);
    $proc.out.lines;
}

method _ssh_options {
    my $proc = run( :err, $!ssh, '-V' );
    my $ssh_version = $proc.err.get;
    if ( $ssh_version ~~ m/ OpenSSH <[-|_]> (\w+) \. / && $0 == 1 ) {
        $.equalspace = " ";
    } else {
        $.equalspace = "=";
    }
    my @options = '-o', 'BatchMode' ~ $.equalspace ~ 'yes';
    if ( $ssh_version ~~ m/ OpenSSH <[-|_]> (\w+) \. / && $0 > 1 ) {
        unshift @options, '-T';
    }
    @options;
}

=begin pod

=head1 NAME

Net::SSH - A Perl 6 wrapper around the command-line ssh utility.

=head1 VERSION

Version 0.01

=head1 SYNOPSIS

The module provides an object method which runs a command on a remote server
via SSH and returns the output of the command.

    use Net::SSH;

    my $client = Net::SSH.new;
    my @lines = $client.ssh-lines('remote-host','command','-option1','-option2');

=head1 SUBROUTINES/METHODS

=head2 ssh-lines

Connects to the remote-host specified and runs the command and options provided.
Returns the output of the command as an array of lines.

=head1 AUTHOR

Aaron Baugher, C<< <aaron at baugher.biz> >>

=head1 BUGS

Please report any bugs or feature requests by creating Issues at 
L<https://gitlab.com/aaron-baugher/p6-net-ssh/>.  Thank you in
advance for any input.

=head1 ACKNOWLEDGEMENTS

I borrowed shamelessly from the Perl 5 module Net::SSH, but all bad code here is mine.

=head1 LICENSE AND COPYRIGHT

Copyright 2015 Aaron Baugher.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=end pod
