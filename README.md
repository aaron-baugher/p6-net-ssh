Net-SSH

This is a simple wrapper around ssh, which runs a command through ssh and returns the output.

INSTALLATION

To install this module, copy Net/SSH.pm6 into wherever your Perl 6 module tree is.
See the POD documentation in the module for usage instructions.

SUPPORT AND DOCUMENTATION

After installing, use your favorite POD reader.

    p6doc Net::SSH

You can also get the latest information at the repository: https://gitlab.com/aaron-baugher/p6-net-ssh/



